public class Board{
	
	private Die firstRoll;
	private Die secondRoll;
	private boolean[] closedTiles;
	
	
	public Board(){
		this.firstRoll = new Die();
		this.secondRoll = new Die();
		this.closedTiles = new boolean[13];
	}
	
	public String toString(){
		String builder = "";
		
		for(int i =1; i < this.closedTiles.length; i++){
			if(this.closedTiles[i] == false){
				builder+=i + ",";
			}
			else { 
				builder+="x" + ",";
			}
		}
		return builder;
	}
	
	public boolean playATurn(){
		
		this.firstRoll.roll();
		this.secondRoll.roll();
		
		System.out.println(firstRoll);
		System.out.println(secondRoll);
		
		int sum = firstRoll.getPips() + secondRoll.getPips();
		
		if(this.closedTiles[sum] == false){
			this.closedTiles[sum] = true;	
			System.out.println("Closing tile :" + sum);
			return false;
		}
		
		else{
			System.out.println("Tile is already shut");
			return true;
		}
		
	}
	
	
	
}
	

	

	
